# -*- coding: utf-8 -*-
from django import forms

from .models import SignUp

class ContactForm(forms.Form):
	full_name = forms.CharField(required=False, label='Ваше имя')
	email = forms.EmailField()
	message = forms.CharField(label='Текст сообщения')


class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields = ['full_name', 'email']
		### exclude = ['full_name']
	
	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		# if domain == 'mail':
		# 	raise forms.ValidationError("мы не работаем с этим email-провайдером")
		# if not extension == "com":
		# 	raise forms.ValidationError("Please use a valid .com email address")
		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		#write validation code.
		return full_name