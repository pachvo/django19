# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render

from .forms import ContactForm, SignUpForm
from .models import SignUp

def home(request):
	title = 'ОСТАВЬТЕ ЗАЯВКУ'
	form = SignUpForm(request.POST or None)
	context = {
		"title": title,
		"form": form
	}
	if form.is_valid():
		#form.save()
		#print request.POST['email'] #not recommended
		instance = form.save(commit=False)

		full_name = form.cleaned_data.get("full_name")
		if not full_name:
			full_name = "anonymous"
		instance.full_name = full_name
		instance.save()
		context = {
			"title": "Ваша заявка принята!"
		}

	if request.user.is_authenticated() and request.user.is_staff:
		#print(SignUp.objects.all())
		# i = 1
		# for instance in SignUp.objects.all():
		# 	print(i)
		# 	print(instance.full_name)
		# 	i += 1

		queryset = SignUp.objects.all().order_by('-timestamp') #.filter(full_name__iexact="Имя")
		#print(SignUp.objects.all().order_by('-timestamp').filter(full_name__iexact="Точное_имя").count())
		context = {
			"queryset": queryset
		}

	return render(request, "newsletter/home.html", context)



def contact(request):
	title = 'Напишите нам'
	title_align_center = True
	form = ContactForm(request.POST or None)
	if form.is_valid():
		# for key, value in form.cleaned_data.iteritems():
		# 	print key, value
		# 	#print form.cleaned_data.get(key)
		form_email = form.cleaned_data.get("email")
		form_message = form.cleaned_data.get("message")
		form_full_name = form.cleaned_data.get("full_name")
		# print email, message, full_name
		subject = 'Сообщение из формы обратной связи на сайте django18'
		from_email = settings.EMAIL_HOST_USER
		to_email = [from_email, 'rso.just@gmail.com']
		contact_message = "%s: %s via %s"%( 
				form_full_name, 
				form_message, 
				form_email)
		some_html_message = "<h2>%s пишет:</h2><p>%s</p><p>%s</p>"%(form_full_name, form_message, form_email)
		send_mail(subject, 
				contact_message, 
				from_email, 
				to_email, 
				html_message=some_html_message,
				fail_silently=True)

	context = {
		"form": form,
		"title": title,
		"title_align_center": title_align_center,
	}
	return render(request, "newsletter/forms.html", context)

def about(request):
	return render(request, "newsletter/about.html", {})
















